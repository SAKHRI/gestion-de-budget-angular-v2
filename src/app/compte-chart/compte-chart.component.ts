import { Component, OnInit, ViewChild } from '@angular/core';
import { TabCompteDataSource } from '../tab-compte/tab-compte-datasource';
import { OperationService } from '../services/operation.service';
import { MatPaginator, MatSort } from '@angular/material';

@Component({
  selector: 'app-compte-chart',
  templateUrl: './compte-chart.component.html',
  styleUrls: ['./compte-chart.component.css']
})
export class CompteChartComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: TabCompteDataSource;


  credit: number = null;
  debit: number = null;
  total: number = null;


  constructor(private service: OperationService) { }

  ngOnInit() {

    this.service.findAll().subscribe(data => {
      this.dataSource = new TabCompteDataSource(this.paginator, this.sort, data);

      //loop data
      for (let operation of data) {
        if (!operation.type) {
          this.credit += operation.price
        } else {
          this.debit += operation.price
        }
      }
      this.pieChartLabels = ['Credit : '+this.credit, 'Debit : '+this.debit]
      this.pieChartData = [this.credit, this.debit];
    })
      ;
  }



  public pieChartLabels: string[];
  public pieChartData: number[];
  public pieChartType: string = 'pie';

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }


}
