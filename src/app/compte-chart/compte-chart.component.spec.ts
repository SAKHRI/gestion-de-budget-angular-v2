import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompteChartComponent } from './compte-chart.component';

describe('CompteChartComponent', () => {
  let component: CompteChartComponent;
  let fixture: ComponentFixture<CompteChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompteChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompteChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
