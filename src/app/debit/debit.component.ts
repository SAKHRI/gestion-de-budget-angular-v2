import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Operation } from '../entity/operation';
import { OperationService } from '../services/operation.service';

@Component({
  selector: 'app-debit',
  templateUrl: './debit.component.html',
  styleUrls: ['./debit.component.css']
})
export class DebitComponent implements OnInit {

  operations: Observable<Operation[]>;
  newOperation: Operation = {name: null, price:null, type: true , category: null, date:null};
  selected: Operation;



  constructor(private service:OperationService) { }

  ngOnInit() {
    this.operations = this.service.findAll();
  }


  isPassed(date:Date) {
    let now = new Date();
    
    return date.getTime() < now.getTime();
  }

}

