import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { TabDebitDataSource } from './tab-debit-datasource';
import { OperationService } from '../services/operation.service';
import { Operation } from '../entity/operation';

@Component({
  selector: 'app-tab-debit',
  templateUrl: './tab-debit.component.html',
  styleUrls: ['./tab-debit.component.css'],
})
export class TabDebitComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: TabDebitDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'date', 'name', 'category', 'price', 'button'];
  row:Operation;

  constructor(private service:OperationService){}


  credit: number = null;
  debit: number = null;
  total: number = null;

  ngOnInit() {
    this.service.findAll().subscribe(data => {
      this.dataSource = new TabDebitDataSource(this.paginator, this.sort, data);
  
        //loop data
        for (let operation of data) {
          if (!operation.type) {
            this.credit += operation.price
          } else {
            this.debit += operation.price
          }
        }
      })
        ;
    }


  deleteOperation(row) {
    this.service.delete(row.id)
      .subscribe(() => {
        this.ngOnInit();

      });
  }
}

