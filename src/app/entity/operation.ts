import { Category } from "./category";

export interface Operation {


  id?: number;
  name: string;
  price: number;
  type?: boolean;
  date?: any;
  category?: Category;

}



